
{{ if { user:group_id user_id= _user:id } == '3' }}

{{ user:profile user_id= _user:id }}

	<div id="main" style="height:120px;">
		<div class="profilelogo"><img src="{{ avatar:image }}" /></div>
		<div class="profilename">
			<h2>{{ user:display_name user_id= _user:id }}</h2>
			<font class="tagline">{{ user:tagline user_id= _user:id }}</font>
		</div>
	</div>
	
	<div id="main">
		<div class="profilebanner">
			<img src="{{ banner_utama:image }}" class="bannerimg" />
		</div>
		<div class="profilebannerbutton">
			<a href=""><img src="{{ theme:path }}/css/img/button-profile-musttry.png" /></a>
			<a href=""><img src="{{ theme:path }}/css/img/button-profile-delivery.png" style="margin:0px 0px 0px -4px" /></a>
		</div>
		<div class="profilebanner2">
			<center>
				<h2>TAMPILKAN<br>PROMO ANDA<br>DISINI</h2>
				<a href="{{ url:site }}paket"><img src="{{ theme:path }}/css/img/button-promo-tampil.png" /></a>
			</center>
		</div>
	</div>
	
	<div style="background:#fff; display:table; padding:10px;">
	<div class="profilebanner">
		<div class="gmap">
			<a href=""><img src="{{ theme:path }}/css/img/gmap.png" /></a>
		</div>
		<div class="detail">
		{{ user:display_name user_id= _user:id }}
		<br><br>
		<div class="det address">{{ user:alamat user_id= _user:id }}</div>
		<div class="det telp">{{ user:telepon user_id= _user:id }}</div>
		<div class="det fb"><a href="{{ user:facebook user_id= _user:id }}" target="_blank">{{ user:facebook user_id= _user:id }}</a></div>
		<div class="det twit"><a href="{{ user:twitter user_id= _user:id }}" target="_blank">{{ user:twitter user_id= _user:id }}</a></div>
		<div class="det bb">{{ user:blackberry_pin user_id= _user:id }}</div>
		<div class="det email">{{ email }}</div>
		<div class="det price">{{ user:harga_range user_id= _user:id }}</div>
		</div>
	</div>
	<div class="topmenu">
		<img src="{{ theme:path }}/css/img/topmenu.png" />
		<img src="{{ menu_photo:image }}" />
	</div>
	<div class="topmenu">
	<h2 style="font-size:17px;">{{ user:nama_menu user_id= _user:id }}</h2>
	{{ user:deskripsi user_id= _user:id }}
	<h2 style="font-size:17px;">Rp {{ user:harga user_id= _user:id }}</h2>
	</div>
	</div>	
	
</div>

<div id="wrapper">
	<div id="main">
		<img src="{{ theme:path }}/css/img/img-mainsec3.png" />
		<br><br><br><br>
		<img src="{{ theme:path }}/css/img/img-mainsec4.png" />
		<br><br><br><br>
		<img src="{{ theme:path }}/css/img/img-mainsec5.png" />
		<br><br><br><br>
	</div>
</div>
{{ /user:profile }}

{{ else }}

{{ user:profile user_id= _user:id }}
<div class="mainside">
	<div id="main" style="height:120px;">
		<div class="profilelogo"><img src="{{ avatar:image }}" /></div>
		<div class="profilename">
			<h2>{{ user:display_name user_id= _user:id }}</h2>
			<font class="tagline" style="font-size:12px;">{{ user:kota_domisili user_id= _user:id }}</font>
			<div style="margin:10px 0px;">{{ user:bio user_id= _user:id }}</div>
			<font class="tagline" style="font-size:12px;">Share stories since  {{ user:created_on user_id= _user:id }}</font>
		</div>
	</div>
	<h3>Stories</h3>
</div>

<div class="rightside">
 	<h3>Popular Writers</h3>
</div>
	
{{ /user:profile }}
{{ endif }}
